# Presentation


Hello! I'm Andres Cedeño, a student of software development technology at Pontificia Universidad Catolica del Ecuador.

## About Me
I'm passionate about technology and software development. Since I was young, I've been intrigued by how technology can transform our lives and improve the way we do things. This passion led me to choose a career in software development, where I've been constantly exploring and learning about new technologies, methodologies, and trends in the industry.

## Experience and Skills
During my time at university, I've had the opportunity to work on a variety of projects, from small applications to more complex systems. These experiences have allowed me to develop skills in different areas of software development, including:

- Web application development.
- Programming in various languages such as Java, Python, JavaScript, among others.
- Database design and management using database management systems.
- Implementation of agile development practices and methodologies such as Scrum and Kanban.
- Collaboration in multidisciplinary teams to achieve common goals.
- Problem-solving and ability to quickly learn new technologies.

## Project Portfolio
I invite you to review some of the projects I've worked on during my time at university. These projects showcase my skills and experiences in software development.

## Interests
Apart from my studies, I'm also interested in exploring new emerging technologies such as artificial intelligence, machine learning, and cloud computing. I firmly believe in the power of technology to drive innovation and solve real-world problems.

## Future Goals
As a student of software development technology, my main goal is to continue learning and improving my skills to become a highly competent software developer. I also aspire to contribute to the field of technology by creating innovative solutions and collaborating on meaningful projects that have a positive impact on society.

## Contact
If you'd like to get in touch with me to collaborate on projects, discuss job opportunities, or simply chat about technology, feel free to contact me via email: andres.cede2001@gmail.com.